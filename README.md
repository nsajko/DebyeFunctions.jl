# DebyeFunctions

[![PkgEval](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/D/DebyeFunctions.svg)](https://JuliaCI.github.io/NanosoldierReports/pkgeval_badges/D/DebyeFunctions.html)

Implements some Debye functions, defined as:

```math
{D_n\left(x\right)} = {{\frac{n}{x^n}} \cdot {\int_0^x {{\frac{t^n}{e^t - 1}} dt}}}
```

Currently the only tested values of ``n`` are integers from one to four.

## Usage examples

### `Float64`

The `Evaluate` submodule currently contains `Float64`-specific functionality. Use
`DebyeFunctions.Evaluate.evaluate(x, Val(n))` to calculate ``D_n(x)``, where `x` is
a `Float64` value.

```julia-repl
julia> import DebyeFunctions

julia> DebyeFunctions.Evaluate.evaluate(-1.3, Val(3))
1.5703513708666639

julia> DebyeFunctions.Evaluate.evaluate(0.0, Val(3))
1.0

julia> DebyeFunctions.Evaluate.evaluate(9.0, Val(3))
0.026199892587964884
```

### Generic

The `DebyeQuadrature` submodule contains an implementation of the Debye functions
that can support any floating-point number type.

```julia-repl
julia> import DebyeFunctions

julia> relative_tolerance = 2.0^-200
6.223015277861142e-61

julia> quadrature_order = 20
20

julia> DebyeFunctions.DebyeQuadrature.evaluate(big"-1.3", Val(3), relative_tolerance, quadrature_order)
1.570351370866663802241585778821552067132056646808002008054568886589056000865476
```
