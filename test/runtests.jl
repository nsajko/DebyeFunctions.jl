setprecision(BigFloat, 64*3)

import DebyeFunctions
using Test

allocated(x, n::Val) = @allocated DebyeFunctions.Evaluate.evaluate(x, n)::Float64

function relative_error(x::Float64, n::Val)
  app = DebyeFunctions.Evaluate.evaluate(x, n)::Float64
  acc = DebyeFunctions.DebyeQuadrature.evaluate(BigFloat(x), n, 2.0^-(64*3 - 5), 70)
  abs(1 - app/acc)
end

function test_order(n::Val)
  max_allowed_rel_err = 7e-16
  for x ∈ range(-100.0, stop = 100.0, length = 1000)
    @test relative_error(x, n) ≤ max_allowed_rel_err
    ((-1.0 ≤ x ≤ 1.0) | (100 ≤ x)) &&
      @test iszero(allocated(x, n))
  end
end

@testset "DebyeFunctions.jl" begin
  @testset "order: $order" for order ∈ 1:4
    test_order(Val(order))
  end
end
