# Copyright © 2024 Neven Sajko. All rights reserved.

module Master

const total_sig_bits = 256
const integ_sig_bits = total_sig_bits - 1
const lp_fp_sig_bits = integ_sig_bits - 40
const quadrature_order = 50

setprecision(BigFloat, total_sig_bits)
import FindMinimaxPolynomial, Tulip, MathOptInterface
include("../src/DebyeQuadrature.jl")
(precision(BigFloat) == total_sig_bits) || error("unexpected")

floating_point_relative_tolerance(sig_bits) = BigFloat(2)^-sig_bits

function tulip_make_lp()
  rtol = floating_point_relative_tolerance(lp_fp_sig_bits)
  MOI = MathOptInterface
  lp = Tulip.Optimizer{BigFloat}()
  MOI.set(lp, MOI.RawOptimizerAttribute("IPM_IterationsLimit"), 500)
  MOI.set(lp, MOI.RawOptimizerAttribute("IPM_TolerancePFeas"),  rtol)
  MOI.set(lp, MOI.RawOptimizerAttribute("IPM_ToleranceDFeas"),  rtol)
  MOI.set(lp, MOI.RawOptimizerAttribute("IPM_ToleranceIFeas"),  rtol)
  MOI.set(lp, MOI.RawOptimizerAttribute("IPM_ToleranceRGap"),   rtol)
  MOI.set(lp, MOI.RawOptimizerAttribute("Presolve_Level"),      1)
  lp
end

function debye(x, dord::Val)
  rel_tol = floating_point_relative_tolerance(integ_sig_bits)
  DebyeQuadrature.evaluate(x, dord, rel_tol, quadrature_order)
end

new_monomials(count::Int) = 0:(count - 1)

new_monomials(c1::Int, c2::Int) = vcat(0:(c1 - 1), range(start = c1 + 1, step = 2, length = c2))

function mmx_impl(::V, monomials, interval, exacts) where {V<:Val}
  deb = x -> debye(x, V())

  MMX = FindMinimaxPolynomial.Minimax

  opts = MMX.minimax_options(
    exact_points = exacts,
    mmx_error_roots_granularity = 14,

    # don't need the "tiny filter" because there are no roots
    tiny_filter_roots_granularity = 1,
  )

  mmx_pol = MMX.minimax_polynomial
  mmx_pol(tulip_make_lp, deb, [interval], monomials, opts)
end

function mmx_around_zero(::V, monomial_count, interval) where {V<:Val}
  (first(interval) < false < last(interval)) || error("unexpected")
  monomials = new_monomials(3, monomial_count - 3)
  exacts = (false,)
  mmx_impl(V(), monomials, interval, exacts)
end

function mmx_away_from_zero(::V, monomial_count, interval) where {V<:Val}
  monomials = new_monomials(monomial_count)
  exacts = ()
  mmx_impl(V(), monomials, interval, exacts)
end

function find_coefficients(debye_order::Val{DebyeOrder}, monomial_count, interval) where {DebyeOrder}
  DebyeOrder::Int
  itv = map(BigFloat, interval)

  mmx = if first(interval) ≤ 0 ≤ last(interval)
    mmx_around_zero(   debye_order, monomial_count, itv)
  else
    mmx_away_from_zero(debye_order, monomial_count, itv)
  end

  (; mmx, interval, DebyeOrder)
end

end
