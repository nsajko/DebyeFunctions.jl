# Copyright © 2024 Neven Sajko. All rights reserved.

module DebyeQuadrature

import QuadGK, ..Power

function evaluate(
  x, dord::Val{DOrd}, relative_tolerance, quadrature_order::Int,
) where {DOrd}
  f = t -> Power.pow(t, dord)/expm1(t)
  if iszero(x)
    one(x)
  else
    let z = false
      i = QuadGK.quadgk(
        f, z, x, maxevals = Inf, atol = z, rtol = relative_tolerance,
        order = quadrature_order,
      )
      DOrd / Power.pow(x, dord) * first(i)
    end
  end
end

end
