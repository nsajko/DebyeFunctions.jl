# Copyright © 2024 Neven Sajko. All rights reserved.

module AsymptoticApproximation

import ..Power

"""
    c(::Val)

```
n * factorial(n) * zeta(n + 1)
```
"""
function c end

c(::Val{1}) = 1.6449340668482264
c(::Val{2}) = 4.808227612638377
c(::Val{3}) = 19.481818206800487
c(::Val{4}) = 99.54506449376352

boundary(::Val{1}) = 41
boundary(::Val{2}) = 45
boundary(::Val{3}) = 46
boundary(::Val{4}) = 51

evaluate(x::Float64, n::Val) = c(n) / Power.pow(x, n)

end
