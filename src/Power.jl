# Copyright © 2024 Neven Sajko. All rights reserved.

module Power

import MutableArithmetics

pow_(x, ::Val{1}) = x
pow_(x, ::Val{2}) = x*x
pow_(x, ::Val{3}) = x^3
pow_(x, ::Val{4}) = x^4

function pow_(x::BigFloat, ::Val{3})
  o! = MutableArithmetics.operate!
  y = x*x
  o!(*, y, x)::BigFloat
end

function pow_(x::BigFloat, ::Val{4})
  o! = MutableArithmetics.operate!
  y = x*x
  o!(*, y, y)::BigFloat
end

pow(x, n::Union{Val{1},Val{2},Val{3},Val{4}}) = pow_(x, n)

pow(x, n) = x^n

end
