# Copyright © 2024 Neven Sajko. All rights reserved.

module Evaluate

import ..DebyeQuadrature, ..PolynomialApproximation, ..AsymptoticApproximation

function evaluate_polynomial_around_zero(x::Float64, ord::Val)
  coefs = PolynomialApproximation.coefficients(ord)
  coefs::Tuple{Float64,Tuple{Vararg{Float64}}}
  (c1, d) = coefs
  y = x * x
  z = evalpoly(y, d)::Float64
  evalpoly(x, (1.0, c1, z))::Float64
end

function evaluate_quadrature(x::Float64, ord::Val)
  tol = 2.0^-50
  q_ord = 7
  DebyeQuadrature.evaluate(x, ord, tol, q_ord)::Float64
end

function evaluate(x::Float64, ord::Val)
  # Domain splitting: split the real number line into several intervals
  (l, u) = PolynomialApproximation.boundaries(ord)
  if l ≤ x ≤ u
    evaluate_polynomial_around_zero(x, ord)
  else
    if AsymptoticApproximation.boundary(ord) < x
      AsymptoticApproximation.evaluate(x, ord)
    else
      evaluate_quadrature(x, ord)
    end
  end::Float64
end

end
